package modelos.oxxo;

import modelos.oxxo.clases.*;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Oxxo {
    private Bolsero bolsero;
    private Cafetera cafetera;
    private Caja caja1;
    private Caja caja2;
    private Cajero cajero1;
    private Cajero cajero2;
    private Jefe jefe;
    private  Vikingo viking;


    public Oxxo() {
    }

    public Oxxo(Bolsero bolsero, Cafetera cafetera, Caja caja1, Caja caja2, Cajero cajero1, Cajero cajero2, Jefe jefe, Vikingo viking) {
        this.bolsero = bolsero;
        this.cafetera = cafetera;
        this.caja1 = caja1;
        this.caja2 = caja2;
        this.cajero1 = cajero1;
        this.cajero2 = cajero2;
        this.jefe = jefe;
        this.viking = viking;
    }

    public Bolsero getBolsero() {
        return bolsero;
    }

    public void setBolsero(Bolsero bolsero) {
        this.bolsero = bolsero;
    }

    public Cafetera getCafetera() {
        return cafetera;
    }

    public void setCafetera(Cafetera cafetera) {
        this.cafetera = cafetera;
    }

    public Caja getCaja1() {
        return caja1;
    }

    public void setCaja1(Caja caja1) {
        this.caja1 = caja1;
    }

    public Caja getCaja2() {
        return caja2;
    }

    public void setCaja2(Caja caja2) {
        this.caja2 = caja2;
    }

    public Cajero getCajero1() {
        return cajero1;
    }

    public void setCajero1(Cajero cajero1) {
        this.cajero1 = cajero1;
    }

    public Cajero getCajero2() {
        return cajero2;
    }

    public void setCajero2(Cajero cajero2) {
        this.cajero2 = cajero2;
    }

    public Jefe getJefe() {
        return jefe;
    }

    public void setJefe(Jefe jefe) {
        this.jefe = jefe;
    }

    public Vikingo getViking() {
        return viking;
    }

    public void setViking(Vikingo viking) {
        this.viking = viking;
    }
}
