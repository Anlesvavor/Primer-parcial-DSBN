package modelos.oxxo.interfaces;

import modelos.oxxo.enums.Sexo;
import modelos.oxxo.excepciones.RangeException;

public interface Persona {
    public Integer getEdad();
    public void setEdad(Integer edad) throws RangeException;
    public void setSexo(Sexo sexo);
}
