package modelos.oxxo.clases;

import modelos.oxxo.excepciones.RangeException;

import java.util.Random;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public abstract class MaquinaDemoniaca {
    private Double probFallo;
    private Integer diasFuncionando;
    private Boolean esfuncional;

    public MaquinaDemoniaca() {
    }

    public MaquinaDemoniaca(Double probFallo, Integer diasFuncionando, Boolean esfuncional) {
        this.probFallo = probFallo;
        this.diasFuncionando = diasFuncionando;
        this.esfuncional = esfuncional;
    }

    public abstract void servir();

    public Double getProbFallo() {
        return probFallo;
    }

    public  void setProbFallo(){
        double r = new Random().nextGaussian();
        this.probFallo = r;
    }

    public void setProbFallo(Double probFallo) throws RangeException{
        if ( probFallo < 0.10d || probFallo > 1.0d ) throw new RangeException();
        this.probFallo = probFallo;
    }

    public Integer getDiasFuncionando() {
        return diasFuncionando;
    }

    public void setDiasFuncionando(Integer diasFuncionando) throws RangeException{
        if ( diasFuncionando < 0 || diasFuncionando > 20 ) throw new RangeException();
        this.diasFuncionando = diasFuncionando;
    }

    public Boolean getEsfuncional() {
        return esfuncional;
    }

    public void setFuncional(){
        this.esfuncional = (diasFuncionando * probFallo > 1.0d)? Boolean.FALSE : Boolean.TRUE;
    }

    public void setEsfuncional(Boolean esfuncional) {
        this.esfuncional = esfuncional;
    }
}
