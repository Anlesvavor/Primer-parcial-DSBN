package modelos.oxxo.clases;

import modelos.oxxo.enums.Comportamiento;
import modelos.oxxo.enums.Sexo;
import modelos.oxxo.excepciones.RangeException;
import modelos.oxxo.interfaces.Persona;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Bolsero implements Persona {

    private Integer edad;
    private Sexo sexo;
    private Float cuota;
    private Comportamiento comportamiento;



    public Float getCuota() {
        return cuota;
    }

    public void setCuota(Float cuota)  throws RangeException{
        if (cuota < 1) throw new RangeException();
        this.cuota = cuota;
    }

    public Comportamiento getComportamiento() {
        return comportamiento;
    }

    public void setComportamiento(Comportamiento comportamiento) {
        this.comportamiento = comportamiento;
    }

    @Override
    public Integer getEdad() {
        return edad;
    }

    @Override
    public void setEdad(Integer edad) throws RangeException{
        if ( ( edad < 13 || edad > 18) || (edad < 65)  ) throw new RangeException();
        this.edad = edad;
    }

    public Sexo getSexo() {
        return sexo;
    }

    @Override
    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }
}
