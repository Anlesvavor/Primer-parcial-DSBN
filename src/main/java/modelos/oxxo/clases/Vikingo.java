package modelos.oxxo.clases;

import java.util.Random;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Vikingo extends MaquinaDemoniaca{

    private Integer cantSalchichas;
    private Float gradoCoccion;

    public Vikingo() {
    }

    public Vikingo(Integer cantSalchichas, Float gradoCoccion) {
        this.cantSalchichas = cantSalchichas;
        this.gradoCoccion = gradoCoccion;
    }

    public Vikingo(Double probFallo, Integer diasFuncionando, Boolean esfuncional, Integer cantSalchichas, Float gradoCoccion) {
        super(probFallo, diasFuncionando, esfuncional);
        this.cantSalchichas = cantSalchichas;
        this.gradoCoccion = gradoCoccion;
    }

    @Override
    public void servir() {
        Double r = new Random().nextGaussian();
        if ( r > getProbFallo() && r < gradoCoccion && cantSalchichas > 0) {
            System.out.println("*Te mira feo*");
        } else {
            System.out.println("Sirviendo...");
            setCantSalchichas(getCantSalchichas() - 1);
        }
    }


    public Integer getCantSalchichas() {
        return cantSalchichas;
    }

    public void setCantSalchichas(Integer cantSalchichas) {
        this.cantSalchichas = cantSalchichas;
    }

    public Float getGradoCoccion() {
        return gradoCoccion;
    }

    public void setGradoCoccion(Float gradoCoccion) {
        this.gradoCoccion = gradoCoccion;
    }
}
