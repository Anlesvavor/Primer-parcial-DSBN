package modelos.oxxo.clases;

import modelos.oxxo.enums.Cafe;

import java.util.List;
import java.util.Random;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Cafetera extends MaquinaDemoniaca {

    private List<Cafe> cafe;

    public Cafetera() {

    }

    public Cafetera(List<Cafe> cafe) {
        this.cafe = cafe;
    }

    public Cafetera(Double probFallo, Integer diasFuncionando, Boolean esfuncional, List<Cafe> cafe) {
        super(probFallo, diasFuncionando, esfuncional);
        this.cafe = cafe;
    }

    @Override
    public void servir() {
        Double r = new Random().nextGaussian();
        if ( r > getProbFallo() ) {
            System.out.println("COF COF");
        } else {
            System.out.println(String.format("Sirviendo...", cafe.get(Integer.parseInt(String.valueOf(r * cafe.size())))));
        }
    }

    public List<Cafe> getCafe() {
        return cafe;
    }

    public void setCafe(List<Cafe> cafe) {
        this.cafe = cafe;
    }
}
