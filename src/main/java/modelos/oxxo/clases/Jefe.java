package modelos.oxxo.clases;

import modelos.oxxo.enums.Frase;
import modelos.oxxo.enums.Sexo;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Jefe extends Cajero {

    private Double deudaConOxxo;
    private Float gritocidad;
    private Boolean haceInventario;



    public Jefe() {
    }

    public Jefe(Double deudaConOxxo, Float gritocidad, Boolean haceInventario) {
        this.deudaConOxxo = deudaConOxxo;
        this.gritocidad = gritocidad;
        this.haceInventario = haceInventario;
    }

    public Jefe(Frase frase, Sexo sexo, Integer edad, Float tiempoTrabajando, Double salario, Double deudaConOxxo, Float gritocidad, Boolean haceInventario) {
        super(frase, sexo, edad, tiempoTrabajando, salario);
        this.deudaConOxxo = deudaConOxxo;
        this.gritocidad = gritocidad;
        this.haceInventario = haceInventario;
    }

    public Double getDeudaConOxxo() {
        return deudaConOxxo;
    }

    public void setDeudaConOxxo(Double deudaConOxxo) {
        this.deudaConOxxo = deudaConOxxo;
    }

    public Float getGritocidad() {
        return gritocidad;
    }

    public void setGritocidad(Float gritocidad) {
        this.gritocidad = gritocidad;
    }

    public Boolean getHaceInventario() {
        return haceInventario;
    }

    public void setHaceInventario(Boolean haceInventario) {
        this.haceInventario = haceInventario;
    }
}
