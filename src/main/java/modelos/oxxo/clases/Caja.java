package modelos.oxxo.clases;

import java.util.Random;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Caja extends MaquinaDemoniaca{

    private Float volatividad;
    private Double dineroCaja;

    public Caja() {

    }

    public Caja(Float volatividad, Double dineroCaja) {
        this.volatividad = volatividad;
        this.dineroCaja = dineroCaja;
    }

    public Caja(Double probFallo, Integer diasFuncionando, Boolean esfuncional, Float volatividad, Double dineroCaja) {
        super(probFallo, diasFuncionando, esfuncional);
        this.volatividad = volatividad;
        this.dineroCaja = dineroCaja;
    }

    @Override
    public void servir() {
        Double r = new Random().nextGaussian();
        if ( r > getProbFallo() && r > volatividad && dineroCaja < 500) {
            System.out.println("*NO HAY SISTEMA*");
        } else {
            System.out.println("despachando");
        }
    }

    public Float getVolatividad() {
        return volatividad;
    }

    public void setVolatividad(Float volatividad) {
        this.volatividad = volatividad;
    }

    public Double getDineroCaja() {
        return dineroCaja;
    }

    public void setDineroCaja(Double dineroCaja) {
        this.dineroCaja = dineroCaja;
    }
}
