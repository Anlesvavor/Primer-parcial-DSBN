package modelos.oxxo.clases;

import modelos.escuela.Personal;
import modelos.oxxo.enums.Frase;
import modelos.oxxo.enums.Sexo;
import modelos.oxxo.excepciones.RangeException;
import modelos.oxxo.interfaces.Persona;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Cajero implements Persona {
    private Frase frase;
    private Sexo sexo;
    private Integer edad;
    private Float tiempoTrabajando;
    private Double salario; // Aunque al ser un oxxo supongo que no es necesario algotan grande como Double :P

    public Cajero() {
    }

    public Cajero(Frase frase, Sexo sexo, Integer edad, Float tiempoTrabajando, Double salario) {
        this.frase = frase;
        this.sexo = sexo;
        this.edad = edad;
        this.tiempoTrabajando = tiempoTrabajando;
        this.salario = salario;
    }

    public Frase getFrase() {
        return frase;
    }

    public void setFrase(Frase frase) {
        this.frase = frase;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) throws RangeException {
        if ( edad < 18 ) throw new RangeException();
        this.edad = edad;
    }

    public Float getTiempoTrabajando() {
        return tiempoTrabajando;
    }

    public void setTiempoTrabajando(Float tiempoTrabajando) throws RangeException {
        if ( tiempoTrabajando < 0 ) throw new RangeException();
        this.tiempoTrabajando = tiempoTrabajando;
    }

    public Double getSalario() {
        return salario;
    }

    public void setSalario(Double salario) throws RangeException {
        if ( salario < 89 ) throw new RangeException();
        this.salario = salario;
    }
}
