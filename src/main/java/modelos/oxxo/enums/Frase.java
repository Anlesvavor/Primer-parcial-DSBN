package modelos.oxxo.enums;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public enum Frase {
    FRASE1("En la otra caja",0.99f),
    FRASE2("Cual cafe?",0.3f),
    FRASE3("Ya se vencio el recibo",0.90f),
    FRASE4("No tiene los 93.50 pesos?",0.85f),
    FRASE5("Desea redondear",0.75f);

    private String frase;
    private Float fastidosidad;

    Frase(String frase, Float fastidosidad) {
        this.frase = frase;
        this.fastidosidad = fastidosidad;
    }

    public String getFrase() {
        return frase;
    }

    public Float getFastidosidad() {
        return fastidosidad;
    }
}
