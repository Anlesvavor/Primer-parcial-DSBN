package modelos.oxxo.enums;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public enum Sexo {
    MASCULINO,
    FEMENINO;
}
