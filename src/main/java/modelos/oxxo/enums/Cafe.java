package modelos.oxxo.enums;

public enum Cafe {
    VAINILLA,
    CANELA,
    MOKA,
    CHOCOLATE,
    AMERICANO;
}
