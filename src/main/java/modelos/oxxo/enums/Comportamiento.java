package modelos.oxxo.enums;

public enum Comportamiento {
    DOCIL(Boolean.FALSE, Boolean.FALSE, Boolean.TRUE),
    AGRESIVO(Boolean.FALSE, Boolean.TRUE, Boolean.TRUE),
    DESINTERESADO(Boolean.FALSE, Boolean.FALSE, Boolean.FALSE),
    AGRADABLE(Boolean.TRUE, Boolean.FALSE, Boolean.TRUE);

    private Boolean diceAlgoAgradable;
    private Boolean exigeRenumeracion;
    private Boolean enbolsa;

    Comportamiento(Boolean diceAlgoAgradable, Boolean exigeRenumeracion, Boolean enbolsa) {
        this.diceAlgoAgradable = diceAlgoAgradable;
        this.exigeRenumeracion = exigeRenumeracion;
        this.enbolsa = enbolsa;
    }

    public Boolean getExigeRenumeracion() {
        return exigeRenumeracion;
    }

    public Boolean getEnbolsa() {
        return enbolsa;
    }

    public Boolean getDiceAlgoAgradable() {
        return diceAlgoAgradable;
    }
}
