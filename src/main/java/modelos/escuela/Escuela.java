package modelos.escuela;

import modelos.escuela.excepciones.InsuficentObjectsException;
import modelos.escuela.excepciones.NotEnoughFoundsException;
import modelos.escuela.interfaces.InstitucionPublica;

import java.util.List;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 13/03/18
 */
public class Escuela implements InstitucionPublica {

    List<Personal> personal;
    Direccion direccion;
    // No confundir con la clase nombre de las personas
    String nombre;
    Double fondos;
    String origenFondos;

    @Override
    public void setPersonal(List<Personal> personal) throws InsuficentObjectsException{
        if ( personal.size() < 10 ) throw new InsuficentObjectsException();
    }

    @Override
    public List<Personal> getPersonal() {
        return this.personal;
    }

    @Override
    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    @Override
    public Direccion getDireccion() {
        return this.direccion;
    }

    @Override
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String getNombre() {
        return this.nombre;
    }

    @Override
    public Double getFondos() {
        return this.fondos;
    }

    @Override
    public void setFondos(Double cantidad) throws NotEnoughFoundsException {
        if ( cantidad < 200000.0d ){
            throw new NotEnoughFoundsException();
        } else {
            this.fondos = fondos;
        }
    }

    @Override
    public String getOrigenFondos() {
        return origenFondos;
    }

    @Override
    public void setOrigenFondos(String origenFondos) {
        this.origenFondos = origenFondos;
    }

    @Override
    public void agregarFondos(Double cantidad) throws NotEnoughFoundsException {
        if ( cantidad < 1000.0d ) {
            throw new NotEnoughFoundsException();
        }
    }

    @Override
    public void retirarFondos(Double cantidad) throws NotEnoughFoundsException {
        if ( cantidad < 1000.0d ) {
            throw new NotEnoughFoundsException();
        }
    }

}
