package modelos.escuela;

import modelos.escuela.excepciones.InsuficentObjectsException;

import java.util.List;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 14/03/18
 */
public abstract class Edificio {

    private Double tamano;
    private Integer cantidadDePuertas;
    private Integer cantidadDeVentanas;
    private Integer cantidadDeCalefactores;
    private Integer cantidadDeMinisplits;
    private Integer cantidadDePizarrones;

    private Integer cantidadDeTomacorrites;
    private Integer cantidadDeEscritorios;
    private Integer cantidadDeSillas;
    private Integer cantidadDeButacas;

    private List<Alumno> alumnos;
    private List<Personal> personal;

    public Double getTamano() {
        return tamano;
    }

    public void setTamano(Double tamano) throws InsuficentObjectsException {
        if ( tamano < 30.0d ) throw new InsuficentObjectsException();
        this.tamano = tamano;
    }

    public Integer getCantidadDePuertas() {
        return cantidadDePuertas;
    }

    public void setCantidadDePuertas(Integer cantidadDePuertas) throws InsuficentObjectsException {
        if ( cantidadDePuertas < 1 ) throw new InsuficentObjectsException();
        this.cantidadDePuertas = cantidadDePuertas;
    }

    public Integer getCantidadDeVentanas() {
        return cantidadDeVentanas;
    }

    public void setCantidadDeVentanas(Integer cantidadDeVentanas) {
        this.cantidadDeVentanas = cantidadDeVentanas;
    }

    public Integer getCantidadDeCalefactores() {
        return cantidadDeCalefactores;
    }

    public void setCantidadDeCalefactores(Integer cantidadDeCalefactores) {
        this.cantidadDeCalefactores = cantidadDeCalefactores;
    }

    public Integer getCantidadDeMinisplits() {
        return cantidadDeMinisplits;
    }

    public void setCantidadDeMinisplits(Integer cantidadDeMinisplits) {
        this.cantidadDeMinisplits = cantidadDeMinisplits;
    }

    public Integer getCantidadDePizarrones() {
        return cantidadDePizarrones;
    }

    public void setCantidadDePizarrones(Integer cantidadDePizarrones) {
        this.cantidadDePizarrones = cantidadDePizarrones;
    }

    public Integer getCantidadDeTomacorrites() {
        return cantidadDeTomacorrites;
    }

    public void setCantidadDeTomacorrites(Integer cantidadDeTomacorrites) {
        this.cantidadDeTomacorrites = cantidadDeTomacorrites;
    }

    public Integer getCantidadDeEscritorios() {
        return cantidadDeEscritorios;
    }

    public void setCantidadDeEscritorios(Integer cantidadDeEscritorios) {
        this.cantidadDeEscritorios = cantidadDeEscritorios;
    }

    public Integer getCantidadDeSillas() {
        return cantidadDeSillas;
    }

    public void setCantidadDeSillas(Integer cantidadDeSillas) {
        this.cantidadDeSillas = cantidadDeSillas;
    }

    public Integer getCantidadDeButacas() {
        return cantidadDeButacas;
    }

    public void setCantidadDeButacas(Integer cantidadDeButacas) {
        this.cantidadDeButacas = cantidadDeButacas;
    }

    public List<Personal> getPersonal() {
        return personal;
    }

    public void setPersonal(List<Personal> personal) {
        this.personal = personal;
    }
}
