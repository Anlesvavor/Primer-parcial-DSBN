package modelos.escuela;

import modelos.escuela.excepciones.NoNameException;
import modelos.escuela.excepciones.NoNumberRangeException;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 14/03/18
 */
public class Materia {

    private String nombre;
    private Integer grado;
    private Float bimestre1;
    private Float bimestre2;
    private Float bimestre3;
    private Float bimestre4;
    private Float bimestre5;
    private Float promedio;

    private final Float minCalif = 0.0f;
    private final Float maxCalif = 10.0f;
    private final Integer minBimestre = 1;
    private final Integer maxBimestre = 5;
    private final Integer minGrado = 1;
    private final Integer maxGrado = 6;

    public Materia() {
    }

    public Materia(String nombre, Integer grado, Float bimestre1, Float bimestre2, Float bimestre3, Float bimestre4, Float bimestre5) throws NoNumberRangeException, NoNameException {
        setNombre(nombre);
        setGrado(grado);
        setBimestre1(bimestre1);
        setBimestre2(bimestre2);
        setBimestre3(bimestre3);
        setBimestre4(bimestre4);
        setBimestre5(bimestre5);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) throws NoNameException {
        if ( nombre.isEmpty() ) throw new NoNameException();
            this.nombre = nombre;
    }

    public Integer getGrado() {
        return grado;
    }

    public void setGrado(Integer grado) throws NoNumberRangeException {
        if ( validarRango(minGrado, this.grado, maxGrado) ) throw new NoNumberRangeException();
        this.grado = grado;
    }

    public Float getBimestre1() {
        return bimestre1;
    }

    public void setBimestre1(Float bimestre1) throws NoNumberRangeException{
        if ( validarRango(minCalif, this.bimestre1, maxCalif) ) throw new NoNumberRangeException();
        this.bimestre1 = bimestre1;
    }

    public Float getBimestre2() {
        return bimestre2;
    }

    public void setBimestre2(Float bimestre2) throws NoNumberRangeException{
        if ( validarRango(minCalif, this.bimestre2, maxCalif) ) throw new NoNumberRangeException();
        this.bimestre2 = bimestre2;
    }

    public Float getBimestre3() {
        return bimestre3;
    }

    public void setBimestre3(Float bimestre3) throws NoNumberRangeException{
        if ( validarRango(minCalif, this.bimestre3, maxCalif) ) throw new NoNumberRangeException();
        this.bimestre3 = bimestre3;
    }

    public Float getBimestre4() {
        return bimestre4;
    }

    public void setBimestre4(Float bimestre4) throws NoNumberRangeException{
        if ( validarRango(minCalif, this.bimestre4, maxCalif) ) throw new NoNumberRangeException();
        this.bimestre4 = bimestre4;
    }

    public Float getBimestre5() {
        return bimestre5;
    }

    public void setBimestre5(Float bimestre5) throws NoNumberRangeException{
        if ( validarRango(minCalif, this.bimestre5, maxCalif) ) throw new NoNumberRangeException();
        this.bimestre5 = bimestre5;
    }

    public Float getPromedio() {
        promedio = promedio();
        return promedio;
    }

    public void setPromedio(Float promedio) throws NoNumberRangeException{
        if ( validarRango(minCalif, this.promedio, maxCalif) ) throw new NoNumberRangeException();
        this.promedio = promedio;
    }

    private Float promedio(){
        return (bimestre1 + bimestre2 + bimestre3 + bimestre4 + bimestre5)/5;
    }

    private Boolean validarRango(Number n1,  Number x, Number n2){
        Float z1 = n1.floatValue();
        Float y = x.floatValue();
        Float z2 = n2.floatValue();
        return (y >= z1 && y <= z2)? Boolean.TRUE : Boolean.FALSE;
    }

    public Float getMinCalif() {
        return minCalif;
    }

    public Float getMaxCalif() {
        return maxCalif;
    }

    public Integer getMinBimestre() {
        return minBimestre;
    }

    public Integer getMaxBimestre() {
        return maxBimestre;
    }

    public Integer getMinGrado() {
        return minGrado;
    }

    public Integer getMaxGrado() {
        return maxGrado;
    }
}
