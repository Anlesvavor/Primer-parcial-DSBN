package modelos.escuela;

import modelos.escuela.excepciones.NoNameException;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 13/03/18
 */
public class Nombre {
    private String primerNombre;
    private String segundoNombre;
    private String nombresAdicionales;
    private String apellidoPaterno;
    private String apellidoMaterno;

    public Nombre(){

    }

    public Nombre(String primerNombre, String segundoNombre, String nombresAdicionales, String apellidoPaterno, String apellidoMaterno) throws NoNameException {
        if ( primerNombre.isEmpty() || apellidoPaterno.isEmpty() || apellidoMaterno.isEmpty() ) {
            throw new NoNameException();
        }
            this.primerNombre = primerNombre;
            this.segundoNombre = segundoNombre;
            this.nombresAdicionales = nombresAdicionales;
            this.apellidoPaterno = apellidoPaterno;
            this.apellidoMaterno = apellidoMaterno;

    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) throws NoNameException{
        if ( primerNombre.isEmpty() ) throw new NoNameException();
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getNombresAdicionales() {
        return nombresAdicionales;
    }

    public void setNombresAdicionales(String nombresAdicionales) {
        this.nombresAdicionales = nombresAdicionales;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) throws NoNameException{
        if ( apellidoPaterno.isEmpty() ) throw new NoNameException();
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) throws NoNameException{
        if ( apellidoMaterno.isEmpty() ) throw new NoNameException();
        this.apellidoMaterno = apellidoMaterno;
    }
}
