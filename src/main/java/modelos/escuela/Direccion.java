package modelos.escuela;

import modelos.escuela.excepciones.NoStreetException;
import modelos.escuela.excepciones.NoNumberRangeException;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 13/03/18
 */
public class Direccion {
    private String calle;
    private String calle2;
    private Integer numero;

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) throws NoStreetException {
        if ( calle.isEmpty() ) throw new NoStreetException();
        this.calle = calle;
    }

    public String getCalle2() {
        return calle2;
    }

    public void setCalle2(String calle2) {
        this.calle2 = calle2;
    }

    public Integer getNumero() {

        return numero;
    }

    public void setNumero(Integer numero) throws NoNumberRangeException {
        if ( numero.toString().length() > 4 || numero.toString().length() < 1 || numero.toString().isEmpty() ) throw new NoNumberRangeException();
        this.numero = numero;
    }

}
