package modelos.escuela.enums;

public enum Sexo {
    MASCULINO(Boolean.TRUE, Boolean.FALSE),
    FEMENINO(Boolean.TRUE, Boolean.TRUE);

    private Boolean usaPantalon;
    private Boolean usaFalda;

    Sexo(Boolean usaPantalon, Boolean usaFalda){
        this.usaPantalon = usaPantalon;
        this.usaFalda = usaFalda;
    }

    public Boolean getUsaPantalon() {
        return usaPantalon;
    }

    public Boolean getUsaFalda() {
        return usaFalda;
    }
}
