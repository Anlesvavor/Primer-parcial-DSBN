package modelos.escuela.enums;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 13/03/18
 */
public enum Servico {
    ESCUELA("Escuela publica", "Escuela publica que recibe apoyos del gobierno"),
    JUNTADEAGUA("Junta municipal del agua y saneamiento","Una dependencia del municipio encargada del sutido de agua"),
    COMISIONDEELECTRICIDAD("Comision Federal Electricidad", "Es una institución federal encargada del suministro de energía electrica"),
    DIF("Sistema Nacional para el Desarrollo Integral de la Familia", "Es un sistema orientado a promocionar la asistencia social");

    private String nombre;
    private String descripcion;

    Servico(String nombre, String descripcion){
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

}
