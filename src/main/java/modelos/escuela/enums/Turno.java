package modelos.escuela.enums;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 14/03/18
 */
public enum Turno {
    MATUTINO(Boolean.TRUE, Boolean.TRUE),
    VESPERTINO(Boolean.TRUE, Boolean.TRUE),
    NOCTURNO(Boolean.FALSE, Boolean.FALSE),
    NOAPLICA(Boolean.FALSE, Boolean.TRUE);

    Turno(Boolean deEstudiantes, Boolean deEmpleados){
        this.deEstudiantes = deEstudiantes;
        this.deEmpleados = deEmpleados;
    }

    private Boolean deEstudiantes;
    private Boolean deEmpleados;

    public Boolean getDeEstudiantes() {
        return deEstudiantes;
    }

    public Boolean getDeEmpleados() {
        return deEmpleados;
    }
}
