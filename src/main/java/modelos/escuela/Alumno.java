package modelos.escuela;

import modelos.escuela.enums.Sexo;
import modelos.escuela.enums.Turno;

import java.util.List;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 14/03/18
 */
public class Alumno {
    private Nombre nombre;
    private Integer grado;
    private Sexo sexo;
    private Turno turno;
    private List<Materia> materias;

    public Alumno() {

    }

    public Alumno(Nombre nombre, Integer grado, Sexo sexo, Turno turno, List<Materia> materias) {
        this.nombre = nombre;
        this.grado = grado;
        this.sexo = sexo;
        this.turno = turno;
        this.materias = materias;
    }

    public Nombre getNombre() {
        return nombre;
    }

    public void setNombre(Nombre nombre) {
        this.nombre = nombre;
    }

    public Integer getGrado() {
        return grado;
    }

    public void setGrado(Integer grado) {
        this.grado = grado;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public Turno getTurno() {
        return turno;
    }

    public void setTurno(Turno turno) {
        this.turno = turno;
    }

    public List<Materia> getMaterias() {
        return materias;
    }

    public void setMaterias(List<Materia> materias) {
        this.materias = materias;
    }
}
