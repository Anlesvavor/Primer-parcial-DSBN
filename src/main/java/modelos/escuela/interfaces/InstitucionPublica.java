package modelos.escuela.interfaces;

import modelos.escuela.Direccion;
import modelos.escuela.Personal;
import modelos.escuela.excepciones.InsuficentObjectsException;
import modelos.escuela.excepciones.NotEnoughFoundsException;

import java.util.List;

public interface InstitucionPublica {

    public void setPersonal(List<Personal> personal) throws InsuficentObjectsException;
    public List<Personal> getPersonal();

    public void setDireccion(Direccion direccion);
    public Direccion getDireccion();

    public void setNombre(String nombre);
    public String getNombre();

    public Double getFondos();
    public void setFondos(Double cantidad) throws NotEnoughFoundsException;
    public String getOrigenFondos();
    public void setOrigenFondos(String origenFondos);
    public void agregarFondos(Double cantidad) throws NotEnoughFoundsException;
    public void retirarFondos(Double cantidad) throws NotEnoughFoundsException;


}
