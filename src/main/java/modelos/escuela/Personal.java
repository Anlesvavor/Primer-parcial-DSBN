package modelos.escuela;

import java.util.Date;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 13/03/18
 */
public class Personal {
    private Nombre nombre;
    private Date fechaNacimiento;
    private String departamento;
    private Direccion direccion;

    public Personal() {
    }

    public Personal(Nombre nombre, Date fechaNacimiento, String departamento, Direccion direccion) {
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
        this.departamento = departamento;
        this.direccion = direccion;
    }

    public Nombre getNombre() {
        return nombre;
    }

    public void setNombre(Nombre nombre) {
        this.
    nombre = nombre;
}
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
}

    public String getDepartamento() {
        return departamento;

}
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }
}
