package modelos.escuela;

import modelos.escuela.Alumno;
import modelos.escuela.Edificio;
import modelos.escuela.Materia;
import modelos.escuela.Personal;

import java.util.List;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 14/03/18
 */
public class SalonDeClase extends Edificio {
    private List<Alumno> listaAlumnos;
    private Materia materia;
    private Personal maestro;

    public SalonDeClase() {
    }

    public SalonDeClase(List<Alumno> listaAlumnos, Materia materia, Personal maestro) {
        this.listaAlumnos = listaAlumnos;
        this.materia = materia;
        this.maestro = maestro;
    }

    public List<Alumno> getListaAlumnos() {
        return listaAlumnos;
    }

    public void setListaAlumnos(List<Alumno> listaAlumnos) {
        this.listaAlumnos = listaAlumnos;
    }

    public Materia getMateria() {
        return materia;
    }

    public void setMateria(Materia materia) {
        this.materia = materia;
    }

    public Personal getMaestro() {
        return maestro;
    }

    public void setMaestro(Personal maestro) {
        this.maestro = maestro;
    }
}
