package modelos.casa;

import modelos.casa.enums.EstiloPiso;
import modelos.casa.enums.Mueble;

import java.util.List;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 14/03/18
 */
public class Cuarto {
    private Float tamano;
    private Integer ventanas;
    private Integer puertas;
    private EstiloPiso piso;
    private List<Mueble> listaMuebles;

    public Cuarto() {
    }

    public Cuarto(Float tamano, Integer ventanas, Integer puertas, EstiloPiso piso, List<Mueble> listaMuebles) {
        this.tamano = tamano;
        this.ventanas = ventanas;
        this.puertas = puertas;
        this.piso = piso;
        this.listaMuebles = listaMuebles;
    }

    public Float getTamano() {
        return tamano;
    }

    public void setTamano(Float tamano) {
        this.tamano = tamano;
    }

    public Integer getVentanas() {
        return ventanas;
    }

    public void setVentanas(Integer ventanas) {
        this.ventanas = ventanas;
    }

    public Integer getPuertas() {
        return puertas;
    }

    public void setPuertas(Integer puertas) {
        this.puertas = puertas;
    }

    public EstiloPiso getPiso() {
        return piso;
    }

    public void setPiso(EstiloPiso piso) {
        this.piso = piso;
    }

    public List<Mueble> getListaMuebles() {
        return listaMuebles;
    }

    public void setListaMuebles(List<Mueble> listaMuebles) {
        this.listaMuebles = listaMuebles;
    }
}
