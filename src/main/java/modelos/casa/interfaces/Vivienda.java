package modelos.casa.interfaces;

import modelos.casa.Bano;
import modelos.casa.Cocina;
import modelos.casa.Cuarto;

public interface Vivienda {

    public Bano getBano();
    public Cocina getCocina();
    public Cuarto getCuarto();

}
