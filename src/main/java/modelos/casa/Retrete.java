package modelos.casa;

import modelos.casa.excepciones.SizeException;

import java.awt.*;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Retrete {
    private Float volumen;
    private Color color;
    private Boolean tapado;

    public Retrete() {
    }

    public Retrete(Float volumen, Color color, Boolean tapado) {
        this.volumen = volumen;
        this.color = color;
        this.tapado = tapado;
    }

    public Float getVolumen() {
        return volumen;
    }

    public void setVolumen(Float volumen) throws SizeException{
        if ( volumen < 0 ) throw new SizeException();
        this.volumen = volumen;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Boolean getTapado() {
        return tapado;
    }

    public void setTapado(Boolean tapado) {
        this.tapado = tapado;
    }
}
