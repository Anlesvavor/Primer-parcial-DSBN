package modelos.casa;

import java.awt.*;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Lavamano extends Regadera{

    private Color color;
    private Boolean jabon;

    public Lavamano(Color color, Boolean jabon) {
        this.color = color;
        this.jabon = jabon;
    }

    public Lavamano() {

    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Boolean getJabon() {
        return jabon;
    }

    public void setJabon(Boolean jabon) {
        this.jabon = jabon;
    }
}
