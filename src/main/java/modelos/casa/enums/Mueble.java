package modelos.casa.enums;

public enum Mueble {
    ESCRITORIO,
    SILLA,
    CAMA,
    MESA,
    REPISA,
    MESADENOCHE,
    BANCO,
    COMPUTADORA,
    MICROONDAS,
    REFRIGERADOR,
    ESTUFA,
    TELEVISOR;
}
