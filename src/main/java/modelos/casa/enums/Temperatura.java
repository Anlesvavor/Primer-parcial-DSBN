package modelos.casa.enums;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public enum Temperatura {
    C120(Boolean.TRUE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE),
    C180(Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE),
    C200(Boolean.FALSE, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE),
    C220(Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE),
    c245(Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE);

    private Boolean flojo;
    private Boolean medio;
    private Boolean medioAlto;
    private Boolean caliente;
    private Boolean muyCaliete;

    Temperatura(Boolean flojo, Boolean medio, Boolean medioAlto, Boolean caliente, Boolean muyCaliete) {
        this.flojo = flojo;
        this.medio = medio;
        this.medioAlto = medioAlto;
        this.caliente = caliente;
        this.muyCaliete = muyCaliete;
    }

    public Boolean getFlojo() {
        return flojo;
    }

    public Boolean getMedio() {
        return medio;
    }

    public Boolean getMedioAlto() {
        return medioAlto;
    }

    public Boolean getCaliente() {
        return caliente;
    }

    public Boolean getMuyCaliete() {
        return muyCaliete;
    }
}
