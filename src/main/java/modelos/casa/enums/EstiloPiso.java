package modelos.casa.enums;

public enum EstiloPiso {
    MADERA,
    CERAMICA,
    MARMOL,
    CEMENTO,
    HORMIGON,
    MOSAICO,
    PIDRAS,
    LOSETAS,
    FLOTANTES;
}
