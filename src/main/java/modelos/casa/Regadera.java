package modelos.casa;

import modelos.casa.excepciones.RangeException;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 14/03/18
 */
public class Regadera {

    private Boolean encendida;
    private Float giroPerillaFria;
    private Float giroPerillaCaliente;
    private Float caudalRegadera;
    private Float caudalDrenaje;



    public Boolean getEncendida() {
        return encendida;
    }

    public void setEncendida(Boolean encendida) {
        this.encendida = encendida;
    }

    public Float getGiroPerillaFria() {
        return giroPerillaFria;
    }

    public void setGiroPerillaFria(Float giroPerillaFria) throws RangeException{
        if ( giroPerillaFria < 0 )  throw new RangeException();
        this.giroPerillaFria = giroPerillaFria;
    }

    public void incrementarPerrillaFria(Float giro) throws RangeException{
        if ( giroPerillaFria + giro < 0) throw new RangeException();
        this.giroPerillaFria += giro;
    }

    public Float getGiroPerillaCaliente() {
        return giroPerillaCaliente;
    }

    public void setGiroPerillaCaliente(Float giroPerillaCaliente) {
        this.giroPerillaCaliente = giroPerillaCaliente;
    }

    public void incrementarPerrillaCaliente(Float giro) throws RangeException{
        if ( giroPerillaCaliente + giro < 0) throw new RangeException();
        this.giroPerillaCaliente += giro;
    }

    public Float getCaudalRegadera() {
        return caudalRegadera;
    }

    public void setCaudalRegadera(Float caudalRegadera) {
        this.caudalRegadera = caudalRegadera;
    }

    public Float getCaudalDrenaje() {
        return caudalDrenaje;
    }

    public void setCaudalDrenaje(Float caudalDrenaje) {
        this.caudalDrenaje = caudalDrenaje;
    }
}
