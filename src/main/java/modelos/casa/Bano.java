package modelos.casa;

import modelos.casa.enums.EstiloPiso;
import modelos.casa.enums.Mueble;

import java.util.List;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 14/03/18
 */
public class Bano extends Cuarto{
    private Retrete retrete;
    private Regadera regadera;
    private Lavamano lavamano;

    public Bano() {
    }

    public Bano(Retrete retrete, Regadera regadera, Lavamano lavamano) {
        this.retrete = retrete;
        this.regadera = regadera;
        this.lavamano = lavamano;
    }

    public Bano(Float tamano, Integer ventanas, Integer puertas, EstiloPiso piso, List<Mueble> listaMuebles, Retrete retrete, Regadera regadera, Lavamano lavamano) {
        super(tamano, ventanas, puertas, piso, listaMuebles);
        this.retrete = retrete;
        this.regadera = regadera;
        this.lavamano = lavamano;
    }

    public Retrete getRetrete() {
        return retrete;
    }

    public void setRetrete(Retrete retrete) {
        this.retrete = retrete;
    }

    public Regadera getRegadera() {
        return regadera;
    }

    public void setRegadera(Regadera regadera) {
        this.regadera = regadera;
    }

    public Lavamano getLavamano() {
        return lavamano;
    }

    public void setLavamano(Lavamano lavamano) {
        this.lavamano = lavamano;
    }
}
