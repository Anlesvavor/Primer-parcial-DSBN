package modelos.casa;

import modelos.casa.excepciones.RangeException;
import modelos.casa.enums.Temperatura;

import java.awt.*;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Estufa {
    private Integer numeroHornillas;
    private Color color;
    private Boolean horno;
    private Temperatura temperaturaDelHorno;


    public Estufa() {

    }

    public Integer getNumeroHornillas() {

        return numeroHornillas;
    }

    public void setNumeroHornillas(Integer numeroHornillas) throws RangeException {
        if(numeroHornillas < 1 || numeroHornillas > 6) throw new RangeException();
        this.numeroHornillas = numeroHornillas;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Boolean getHorno() {
        return horno;
    }

    public void setHorno(Boolean horno) {
        this.horno = horno;
    }

    public Temperatura getTemperaturaDelHorno() {
        return temperaturaDelHorno;
    }

    public void setTemperaturaDelHorno(Temperatura temperaturaDelHorno) {
        this.temperaturaDelHorno = temperaturaDelHorno;
    }
}
