package modelos.casa;

import modelos.casa.enums.EstiloPiso;
import modelos.casa.enums.Mueble;

import java.util.List;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 14/03/18
 */
public class Cocina extends Cuarto {
    private Estufa estufa;
    private Fregadero fregadero;

    public Cocina(){

    }

    public Cocina(Estufa estufa, Fregadero fregadero) {
        this.estufa = estufa;
        this.fregadero = fregadero;
    }

    public Cocina(Float tamano, Integer ventanas, Integer puertas, EstiloPiso piso, List<Mueble> listaMuebles, Estufa estufa, Fregadero fregadero) {
        super(tamano, ventanas, puertas, piso, listaMuebles);
        this.estufa = estufa;
        this.fregadero = fregadero;
    }

    public Estufa getEstufa() {
        return estufa;
    }

    public void setEstufa(Estufa estufa) {
        this.estufa = estufa;
    }

    public Fregadero getFregadero() {
        return fregadero;
    }

    public void setFregadero(Fregadero fregadero) {
        this.fregadero = fregadero;
    }
}
