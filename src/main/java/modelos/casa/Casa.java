package modelos.casa;

import modelos.casa.excepciones.InsuficentObjectsException;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 14/03/18
 */
public class Casa {
    private Bano bano;
    private Cocina coina;
    private Cuarto cuarto;

    public Casa() {
    }

    public Casa(Bano bano, Cocina coina, Cuarto cuarto) {
        this.bano = bano;
        this.coina = coina;
        this.cuarto = cuarto;
    }

    public Bano getBano() {
        return bano;
    }

    public void setBano(Bano bano) throws InsuficentObjectsException{
        if ( bano.equals(null) ) throw new InsuficentObjectsException();
        this.bano = bano;
    }

    public Cocina getCoina() {
        return coina;
    }

    public void setCoina(Cocina coina) throws InsuficentObjectsException{
        if ( coina.equals(null)) throw new InsuficentObjectsException();
        this.coina = coina;
    }

    public Cuarto getCuarto() {
        return cuarto;
    }

    public void setCuarto(Cuarto cuarto) throws InsuficentObjectsException{
        if ( cuarto.equals(null) ) throw new InsuficentObjectsException();
        this.cuarto = cuarto;
    }

    public static void main(String[] args) {
        System.out.println("Hola!, Soy Jesús José, y este es un modelo de mi casa, como puedes observar soy pobre, con hambre y con la prisa de hacer otros cinco modelos :33333");
    }
}
