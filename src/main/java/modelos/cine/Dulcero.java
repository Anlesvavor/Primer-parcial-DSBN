package modelos.cine;

import modelos.cine.enums.Sexo;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Dulcero extends Cajero {
    private Float cantidaDeDulces;
    private Float palomitasAlmacenamieto;

    public Dulcero(Double saldoEnCaja, Boolean usaTerminalTarj, Float cantidaDeDulces, Float palomitasAlmacenamieto) {
        super(saldoEnCaja, usaTerminalTarj);
        this.cantidaDeDulces = cantidaDeDulces;
        this.palomitasAlmacenamieto = palomitasAlmacenamieto;
    }

    public Dulcero(Integer edad, Sexo sexo, Float salario, String nombre, Double saldoEnCaja, Boolean usaTerminalTarj, Float cantidaDeDulces, Float palomitasAlmacenamieto) {
        super(edad, sexo, salario, nombre, saldoEnCaja, usaTerminalTarj);
        this.cantidaDeDulces = cantidaDeDulces;
        this.palomitasAlmacenamieto = palomitasAlmacenamieto;
    }

    public Float getCantidaDeDulces() {
        return cantidaDeDulces;
    }

    public void setCantidaDeDulces(Float cantidaDeDulces) {
        this.cantidaDeDulces = cantidaDeDulces;
    }

    public Float getPalomitasAlmacenamieto() {
        return palomitasAlmacenamieto;
    }

    public void setPalomitasAlmacenamieto(Float palomitasAlmacenamieto) {
        this.palomitasAlmacenamieto = palomitasAlmacenamieto;
    }
}
