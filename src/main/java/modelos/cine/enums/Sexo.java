package modelos.cine.enums;

public enum Sexo {
    MASCULINO,
    FEMENINO;
}
