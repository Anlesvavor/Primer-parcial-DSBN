package modelos.cine.enums;

public enum Clasificacion {
    G,
    PG,
    _14A,
    _18A,
    R,
    A;
}
