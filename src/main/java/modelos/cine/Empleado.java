package modelos.cine;

import modelos.cine.enums.Sexo;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Empleado {
    private Integer edad;
    private Sexo sexo;
    private Float salario;
    private String nombre;

    public Empleado() {
    }

    public Empleado(Integer edad, Sexo sexo, Float salario, String nombre) {
        this.edad = edad;
        this.sexo = sexo;
        this.salario = salario;
        this.nombre = nombre;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public Float getSalario() {
        return salario;
    }

    public void setSalario(Float salario) {
        this.salario = salario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
