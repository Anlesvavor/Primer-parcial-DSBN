package modelos.cine;

import modelos.cine.enums.Sexo;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Cajero extends Empleado{
    private Double saldoEnCaja;
    private final Double saldoInicial = 1000.00d;
    private Boolean usaTerminalTarj;

    public Cajero(Double saldoEnCaja, Boolean usaTerminalTarj) {
        this.saldoEnCaja = saldoEnCaja;
        this.usaTerminalTarj = usaTerminalTarj;
    }

    public Cajero(Integer edad, Sexo sexo, Float salario, String nombre, Double saldoEnCaja, Boolean usaTerminalTarj) {
        super(edad, sexo, salario, nombre);
        this.saldoEnCaja = saldoEnCaja;
        this.usaTerminalTarj = usaTerminalTarj;
    }

    public Double getSaldoEnCaja() {
        return saldoEnCaja;
    }

    public void setSaldoEnCaja(Double saldoEnCaja) {
        this.saldoEnCaja = saldoEnCaja;
    }

    public Double getSaldoInicial() {
        return saldoInicial;
    }

    public Boolean getUsaTerminalTarj() {
        return usaTerminalTarj;
    }

    public void setUsaTerminalTarj(Boolean usaTerminalTarj) {
        this.usaTerminalTarj = usaTerminalTarj;
    }
}
