package modelos.cine;

import modelos.cine.enums.Sexo;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Limpieza extends Empleado {

    private Boolean portaEscoba;
    private Boolean portaTrapeador;
    private Boolean portaTrapo;

    public void limpiar(Integer number){
        System.out.println(String.format("Limpiando sala número %d",number));
    }


    public Limpieza(Boolean portaEscoba, Boolean portaTrapeador, Boolean portaTrapo) {
        this.portaEscoba = portaEscoba;
        this.portaTrapeador = portaTrapeador;
        this.portaTrapo = portaTrapo;
    }

    public Limpieza(Integer edad, Sexo sexo, Float salario, String nombre, Boolean portaEscoba, Boolean portaTrapeador, Boolean portaTrapo) {
        super(edad, sexo, salario, nombre);
        this.portaEscoba = portaEscoba;
        this.portaTrapeador = portaTrapeador;
        this.portaTrapo = portaTrapo;
    }

    public Boolean getPortaEscoba() {
        return portaEscoba;
    }

    public void setPortaEscoba(Boolean portaEscoba) {
        this.portaEscoba = portaEscoba;
    }

    public Boolean getPortaTrapeador() {
        return portaTrapeador;
    }

    public void setPortaTrapeador(Boolean portaTrapeador) {
        this.portaTrapeador = portaTrapeador;
    }

    public Boolean getPortaTrapo() {
        return portaTrapo;
    }

    public void setPortaTrapo(Boolean portaTrapo) {
        this.portaTrapo = portaTrapo;
    }
}
