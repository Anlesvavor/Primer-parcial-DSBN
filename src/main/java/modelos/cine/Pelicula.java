package modelos.cine;

import modelos.cine.enums.Clasificacion;

import java.util.Date;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Pelicula {
    private String nobmre;
    private Float duracion;
    private Clasificacion clasificacion;
    private Date fechaDeEstreno;

    public Pelicula(String nobmre, Float duracion, Clasificacion clasificacion, Date fechaDeEstreno) {
        this.nobmre = nobmre;
        this.duracion = duracion;
        this.clasificacion = clasificacion;
        this.fechaDeEstreno = fechaDeEstreno;
    }

    public String getNobmre() {
        return nobmre;
    }

    public void setNobmre(String nobmre) {
        this.nobmre = nobmre;
    }

    public Float getDuracion() {
        return duracion;
    }

    public void setDuracion(Float duracion) {
        this.duracion = duracion;
    }

    public Clasificacion getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(Clasificacion clasificacion) {
        this.clasificacion = clasificacion;
    }

    public Date getFechaDeEstreno() {
        return fechaDeEstreno;
    }

    public void setFechaDeEstreno(Date fechaDeEstreno) {
        this.fechaDeEstreno = fechaDeEstreno;
    }
}
