package modelos.cine;

import modelos.cine.enums.Sexo;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Chequeador extends Empleado {
    private Boolean cheq;

    public Chequeador(Boolean cheq) {
        this.cheq = cheq;
    }

    public Chequeador(Integer edad, Sexo sexo, Float salario, String nombre, Boolean cheq) {
        super(edad, sexo, salario, nombre);
        this.cheq = cheq;
    }

    public Boolean getCheq() {
        return cheq;
    }

    public void setCheq(Boolean cheq) {
        this.cheq = cheq;
    }
}
