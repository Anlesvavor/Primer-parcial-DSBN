package modelos.cine;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Sala {
    private Integer numeroDeAsientos;
    private Integer cantidaDePuertas;
    private Integer cantidaDePuesrtaDeEmergencia;
    private Float longitudDeLaPantalla;
    private Pelicula pelicula;


    public Sala(Integer numeroDeAsientos, Integer cantidaDePuertas, Integer cantidaDePuesrtaDeEmergencia, Float longitudDeLaPantalla, Pelicula pelicula) {
        this.numeroDeAsientos = numeroDeAsientos;
        this.cantidaDePuertas = cantidaDePuertas;
        this.cantidaDePuesrtaDeEmergencia = cantidaDePuesrtaDeEmergencia;
        this.longitudDeLaPantalla = longitudDeLaPantalla;
        this.pelicula = pelicula;
    }

    public Integer getNumeroDeAsientos() {
        return numeroDeAsientos;
    }

    public void setNumeroDeAsientos(Integer numeroDeAsientos) {
        this.numeroDeAsientos = numeroDeAsientos;
    }

    public Integer getCantidaDePuertas() {
        return cantidaDePuertas;
    }

    public void setCantidaDePuertas(Integer cantidaDePuertas) {
        this.cantidaDePuertas = cantidaDePuertas;
    }

    public Integer getCantidaDePuesrtaDeEmergencia() {
        return cantidaDePuesrtaDeEmergencia;
    }

    public void setCantidaDePuesrtaDeEmergencia(Integer cantidaDePuesrtaDeEmergencia) {
        this.cantidaDePuesrtaDeEmergencia = cantidaDePuesrtaDeEmergencia;
    }

    public Float getLongitudDeLaPantalla() {
        return longitudDeLaPantalla;
    }

    public void setLongitudDeLaPantalla(Float longitudDeLaPantalla) {
        this.longitudDeLaPantalla = longitudDeLaPantalla;
    }

    public Pelicula getPelicula() {
        return pelicula;
    }

    public void setPelicula(Pelicula pelicula) {
        this.pelicula = pelicula;
    }
}
