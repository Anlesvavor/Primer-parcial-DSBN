package modelos.cine;

import modelos.cine.enums.Clasificacion;
import modelos.cine.enums.TresD;

import java.util.Date;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Pelicula3d extends Pelicula {

    private Boolean requiereLentes;
    private Boolean aptoParaInfates;
    private TresD modo;

    public Pelicula3d(String nobmre, Float duracion, Clasificacion clasificacion, Date fechaDeEstreno, Boolean requiereLentes, Boolean aptoParaInfates, TresD modo) {
        super(nobmre, duracion, clasificacion, fechaDeEstreno);
        this.requiereLentes = requiereLentes;
        this.aptoParaInfates = aptoParaInfates;
        this.modo = modo;
    }

    public Boolean getRequiereLentes() {
        return requiereLentes;
    }

    public void setRequiereLentes(Boolean requiereLentes) {
        this.requiereLentes = requiereLentes;
    }

    public Boolean getAptoParaInfates() {
        return aptoParaInfates;
    }

    public void setAptoParaInfates(Boolean aptoParaInfates) {
        this.aptoParaInfates = aptoParaInfates;
    }

    public TresD getModo() {
        return modo;
    }

    public void setModo(TresD modo) {
        this.modo = modo;
    }

    public Pelicula3d(String nobmre, Float duracion, Clasificacion clasificacion, Date fechaDeEstreno) {
        super(nobmre, duracion, clasificacion, fechaDeEstreno);
    }
}
