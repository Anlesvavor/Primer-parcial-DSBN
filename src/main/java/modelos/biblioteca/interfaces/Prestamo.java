package modelos.biblioteca.interfaces;

import modelos.biblioteca.clases.Libro;

import java.util.Date;

public interface Prestamo {
    public void prestar(Date inicio, Date fin);
}
