package modelos.biblioteca.clases;

import modelos.biblioteca.interfaces.Prestamo;

import java.util.Date;
import java.util.List;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Libro implements Prestamo {

    private String titulo;
    private List<String> autores;
    private Integer numeroPaginas;
    private Date fechaDepublicacion;
    private Integer publicacion;

    private Date inicio;
    private Date fin;

    public Libro() {
    }

    public Libro(String titulo, List<String> autores, Integer numeroPaginas, Date fechaDepublicacion, Integer publicacion, Date inicio, Date fin) {
        this.titulo = titulo;
        this.autores = autores;
        this.numeroPaginas = numeroPaginas;
        this.fechaDepublicacion = fechaDepublicacion;
        this.publicacion = publicacion;
        this.inicio = inicio;
        this.fin = fin;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public List<String> getAutores() {
        return autores;
    }

    public void setAutores(List<String> autores) {
        this.autores = autores;
    }

    public Integer getNumeroPaginas() {
        return numeroPaginas;
    }

    public void setNumeroPaginas(Integer numeroPaginas) {
        this.numeroPaginas = numeroPaginas;
    }

    public Date getFechaDepublicacion() {
        return fechaDepublicacion;
    }

    public void setFechaDepublicacion(Date fechaDepublicacion) {
        this.fechaDepublicacion = fechaDepublicacion;
    }

    public Integer getPublicacion() {
        return publicacion;
    }

    public void setPublicacion(Integer publicacion) {
        this.publicacion = publicacion;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    @Override
    public void prestar(Date inicio, Date fin) {

    }
}
