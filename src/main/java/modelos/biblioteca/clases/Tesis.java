package modelos.biblioteca.clases;

import modelos.biblioteca.enums.Carrera;
import modelos.biblioteca.interfaces.Prestamo;

import java.util.Date;
import java.util.List;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Tesis extends Libro implements Prestamo{
    private String revisor;
    private String tutor;
    private Carrera carrera;

    public Tesis() {
    }

    public Tesis(String revisor, String tutor, Carrera carrera) {
        this.revisor = revisor;
        this.tutor = tutor;
        this.carrera = carrera;
    }

    public Tesis(String titulo, List<String> autores, Integer numeroPaginas, Date fechaDepublicacion, Integer publicacion, Date inicio, Date fin, String revisor, String tutor, Carrera carrera) {
        super(titulo, autores, numeroPaginas, fechaDepublicacion, publicacion, inicio, fin);
        this.revisor = revisor;
        this.tutor = tutor;
        this.carrera = carrera;
    }

    public String getRevisor() {
        return revisor;
    }

    public void setRevisor(String revisor) {
        this.revisor = revisor;
    }

    public String getTutor() {
        return tutor;
    }

    public void setTutor(String tutor) {
        this.tutor = tutor;
    }

    public Carrera getCarrera() {
        return carrera;
    }

    public void setCarrera(Carrera carrera) {
        this.carrera = carrera;
    }
}
