package modelos.biblioteca.clases;

import modelos.escuela.Direccion;
import modelos.escuela.Nombre;

import java.util.Date;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Personal {
    private Nombre nombre;
    private Date fechaNacimiento;
    private Direccion direccion;

    public Personal() {
    }

    public Personal(Nombre nombre, Date fechaNacimiento, String departamento, Direccion direccion) {
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
        this.direccion = direccion;
    }

    public Nombre getNombre() {
        return nombre;
    }

    public void setNombre(Nombre nombre) {
        this.
                nombre = nombre;
    }
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }
}
