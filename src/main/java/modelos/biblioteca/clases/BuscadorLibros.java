package modelos.biblioteca.clases;

import modelos.biblioteca.enums.SistemaOperativo;
import modelos.biblioteca.excepciones.NoObjectException;

import java.util.List;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class BuscadorLibros extends Computadora {
    private List<Ebook> baseDeDatos;

    public BuscadorLibros(){

    }

    public BuscadorLibros(List<Ebook> baseDeDatos) {
        this.baseDeDatos = baseDeDatos;
    }

    public BuscadorLibros(SistemaOperativo sistemaOperativo, Boolean encendida, Boolean internet, Boolean actualizando, List<Ebook> baseDeDatos) {
        super(sistemaOperativo, encendida, internet, actualizando);
        this.baseDeDatos = baseDeDatos;
    }

    public List<Ebook> getBaseDeDatos() {
        return baseDeDatos;
    }

    public void addLibro(Ebook ebook){
        baseDeDatos.add(ebook);
    }

    public void removeLibro(Ebook ebook) throws  NoObjectException{
        if (!baseDeDatos.contains(ebook)) throw new NoObjectException();
        baseDeDatos.remove(ebook);
    }

    public void setBaseDeDatos(List<Ebook> baseDeDatos) {
        this.baseDeDatos = baseDeDatos;
    }
}
