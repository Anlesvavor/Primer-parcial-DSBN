package modelos.biblioteca.clases;

import modelos.biblioteca.excepciones.NoRangeException;
import modelos.escuela.excepciones.NoNameException;

import java.util.Date;
import java.util.List;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Ebook extends Libro{
    private String url;
    private Double tamano;

    public Ebook(){

    }

    public Ebook(String url, Double tamano) {
        this.url = url;
        this.tamano = tamano;
    }

    public Ebook(String titulo, List<String> autores, Integer numeroPaginas, Date fechaDepublicacion, Integer publicacion, Date inicio, Date fin, String url, Double tamano) {
        super(titulo, autores, numeroPaginas, fechaDepublicacion, publicacion, inicio, fin);
        this.url = url;
        this.tamano = tamano;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) throws NoNameException{
        if ( url.isEmpty() ) throw new NoNameException();
        this.url = url;
    }

    public Double getTamano() {
        return tamano;
    }

    public void setTamano(Double tamano) throws NoRangeException{
        if ( tamano < 0.0 ) throw new NoRangeException();
        this.tamano = tamano;
    }
}
