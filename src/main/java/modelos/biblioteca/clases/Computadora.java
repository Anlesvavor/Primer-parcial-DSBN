package modelos.biblioteca.clases;

import modelos.biblioteca.enums.SistemaOperativo;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Computadora {
    private SistemaOperativo sistemaOperativo;
    private Boolean encendida;
    private Boolean internet;
    private Boolean actualizando;

    public Computadora(){

    }

    public Computadora(SistemaOperativo sistemaOperativo, Boolean encendida, Boolean internet, Boolean actualizando) {
        this.sistemaOperativo = sistemaOperativo;
        this.encendida = encendida;
        this.internet = internet;
        this.actualizando = actualizando;
    }

    public SistemaOperativo getSistemaOperativo() {
        return sistemaOperativo;
    }

    public void setSistemaOperativo(SistemaOperativo sistemaOperativo) {
        this.sistemaOperativo = sistemaOperativo;
    }

    public Boolean getEncendida() {
        return encendida;
    }

    public void setEncendida(Boolean encendida) {
        this.encendida = encendida;
    }

    public Boolean getInternet() {
        return internet;
    }

    public void setInternet(Boolean internet) {
        this.internet = internet;
    }

    public Boolean getActualizando() {
        return actualizando;
    }

    public void setActualizando(Boolean actualizando) {
        this.actualizando = actualizando;
    }
}
