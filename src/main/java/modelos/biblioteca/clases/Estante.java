package modelos.biblioteca.clases;

import modelos.biblioteca.excepciones.NoRangeException;

import java.util.List;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Estante {
    private List<Libro> libros;
    private Integer numero;
    private String Carrera;



    public List<Libro> getLibros() {
        return libros;
    }

    public void setLibros(List<Libro> libros) {
        this.libros = libros;
    }


    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) throws NoRangeException {
        if ( numero < 1 ) throw new NoRangeException();
        this.numero = numero;
    }

    public String getCarrera() {
        return Carrera;
    }

    public void setCarrera(String carrera) {
        Carrera = carrera;
    }
}
