package modelos.biblioteca.enums;

public enum SistemaOperativo {
    LINUX(Boolean.TRUE),
    WINDOWS(Boolean.FALSE),
    MACOS(Boolean.FALSE);

    SistemaOperativo(Boolean libre) {
        this.libre = libre;
    }

    private Boolean libre;

    public Boolean getLibre() {
        return libre;
    }
}
