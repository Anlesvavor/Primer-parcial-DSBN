package modelos.biblioteca.enums;

public enum Carrera {
    CIVIL(Boolean.FALSE,Boolean.FALSE,Boolean.TRUE),
    MINAS(Boolean.TRUE,Boolean.FALSE,Boolean.FALSE),
    GEOLOGÍA(Boolean.TRUE,Boolean.FALSE,Boolean.FALSE),
    HARDWARE(Boolean.FALSE,Boolean.TRUE,Boolean.FALSE),
    CIENCIAS_DE_LA_COMPUTACION(Boolean.FALSE,Boolean.TRUE,Boolean.FALSE),
    TOPOLOGIA(Boolean.TRUE,Boolean.FALSE,Boolean.FALSE),
    SOFTWARE(Boolean.FALSE,Boolean.TRUE,Boolean.FALSE),
    TECNOLOGIA_DE_PROCESO(Boolean.FALSE,Boolean.FALSE,Boolean.TRUE);

    Carrera(Boolean cienciasDeLaTierra, Boolean cienciasComputacionales, Boolean sinClasificar) {
        this.cienciasDeLaTierra = cienciasDeLaTierra;
        this.cienciasComputacionales = cienciasComputacionales;
        this.sinClasificar = sinClasificar;
    }

    private Boolean cienciasDeLaTierra;
    private Boolean cienciasComputacionales;
    private Boolean sinClasificar;

}
