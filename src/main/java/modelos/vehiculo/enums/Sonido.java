package modelos.vehiculo.enums;

public enum Sonido {
    SIN_SONIDO(Boolean.FALSE,Boolean.TRUE),
    FOSIL(Boolean.TRUE, Boolean.FALSE),
    FOSIL_REALISTA(Boolean.TRUE, Boolean.TRUE),
    ELECTRICO(Boolean.FALSE, Boolean.TRUE);

    private Boolean deFosil;
    private Boolean deElectrico;

    Sonido(Boolean deFosil, Boolean deElectrico) {
        this.deFosil = deFosil;
        this.deElectrico = deElectrico;
    }
}
