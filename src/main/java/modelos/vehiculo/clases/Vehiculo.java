package modelos.vehiculo.clases;

import modelos.vehiculo.excepciones.YaEnEseEstadoException;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public abstract class Vehiculo {
    private Integer numeroLlantas;
    private Float cilindraje;
    private Float tanque;
    private Float tanqueCapacidad;
    private Integer numeroPuertas;
    private Integer numeroPasajeros;
    private Boolean encendido;
    private Boolean enMarcha;

    public abstract void arrancar() throws YaEnEseEstadoException;
    public abstract void apagar() throws  YaEnEseEstadoException;
    public abstract void avanzar();
    public abstract void encender() throws  YaEnEseEstadoException;

    public Vehiculo(){

    }

    public Vehiculo(Integer numeroLlantas, Float cilindraje, Float tanque, Integer numeroPuertas, Integer numeroPasajeros) {
        this.numeroLlantas = numeroLlantas;
        this.cilindraje = cilindraje;
        this.tanque = tanque;
        this.numeroPuertas = numeroPuertas;
        this.numeroPasajeros = numeroPasajeros;
    }

    public Boolean getEncendido() {
        return encendido;
    }

    public void setEncendido(Boolean encendido) {
        this.encendido = encendido;
    }

    public Boolean getEnMarcha() {
        return enMarcha;
    }

    public void setEnMarcha(Boolean enMarcha) {
        this.enMarcha = enMarcha;
    }

    public Float getTanqueCapacidad() {
        return tanqueCapacidad;
    }

    public void setTanqueCapacidad(Float tanqueCapacidad) {
        this.tanqueCapacidad = tanqueCapacidad;
    }

    public Integer getNumeroLlantas() {
        return numeroLlantas;
    }

    public void setNumeroLlantas(Integer numeroLlantas) {
        this.numeroLlantas = numeroLlantas;
    }

    public Float getCilindraje() {
        return cilindraje;
    }

    public void setCilindraje(Float cilindraje) {
        this.cilindraje = cilindraje;
    }

    public Float getTanque() {
        return tanque;
    }

    public void setTanque(Float tanque) {
        this.tanque = tanque;
    }

    public Integer getNumeroPuertas() {
        return numeroPuertas;
    }

    public void setNumeroPuertas(Integer numeroPuertas) {
        this.numeroPuertas = numeroPuertas;
    }

    public Integer getNumeroPasajeros() {
        return numeroPasajeros;
    }

    public void setNumeroPasajeros(Integer numeroPasajeros) {
        this.numeroPasajeros = numeroPasajeros;
    }
}
