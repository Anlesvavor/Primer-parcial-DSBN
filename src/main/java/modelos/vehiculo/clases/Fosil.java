package modelos.vehiculo.clases;

import modelos.vehiculo.excepciones.TanqueLlenoExpection;
import modelos.vehiculo.excepciones.YaEnEseEstadoException;
import modelos.vehiculo.interfaces.Energizable;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Fosil extends Vehiculo implements Energizable {

    public Fosil() {
    }

    public Fosil(Integer numeroLlantas, Float cilindraje, Float tanque, Integer numeroPuertas, Integer numeroPasajeros) {
        super(numeroLlantas, cilindraje, tanque, numeroPuertas, numeroPasajeros);
    }

    @Override
    public void llenar() throws TanqueLlenoExpection {
        if (tanqueLleno()) throw new TanqueLlenoExpection();
        setTanque(getTanqueCapacidad());
    }

    @Override
    public Boolean tanqueVacio() {
        return getTanque() == 0.0f? Boolean.TRUE : Boolean.FALSE;
    }

    @Override
    public Boolean tanqueLleno() {
        return getTanque().equals(getTanqueCapacidad()) ? Boolean.TRUE : Boolean.FALSE;
    }

    @Override
    public void arrancar() {
        System.out.println("brrrum");
    }

    @Override
    public void apagar() throws YaEnEseEstadoException {
        if ( getEncendido().equals(Boolean.FALSE) ) throw new YaEnEseEstadoException();
        setEncendido(Boolean.FALSE);
    }

    @Override
    public void avanzar() {
        System.out.println("wssssss");
    }

    @Override
    public void encender() throws YaEnEseEstadoException{
        if( getEncendido().equals(Boolean.TRUE) ) throw new YaEnEseEstadoException();
        setEncendido(Boolean.TRUE);
        System.out.println("cluinwk");
    }
}
