package modelos.vehiculo.clases;

import modelos.vehiculo.enums.Sonido;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Trailer extends Fosil{
    private Float capacidadCarga;
    private Integer cantidadRemolques;
    private Sonido sonido;

    public Trailer(Float capacidadCarga, Integer cantidadRemolques, Sonido sonido) {
        this.capacidadCarga = capacidadCarga;
        this.cantidadRemolques = cantidadRemolques;
        this.sonido = sonido;
    }

    public Trailer(Integer numeroLlantas, Float cilindraje, Float tanque, Integer numeroPuertas, Integer numeroPasajeros, Float capacidadCarga, Integer cantidadRemolques, Sonido sonido) {
        super(numeroLlantas, cilindraje, tanque, numeroPuertas, numeroPasajeros);
        this.capacidadCarga = capacidadCarga;
        this.cantidadRemolques = cantidadRemolques;
        this.sonido = sonido;
    }

    public Float getCapacidadCarga() {
        return capacidadCarga;
    }

    public void setCapacidadCarga(Float capacidadCarga) {
        this.capacidadCarga = capacidadCarga;
    }

    public Integer getCantidadRemolques() {
        return cantidadRemolques;
    }

    public void setCantidadRemolques(Integer cantidadRemolques) {
        this.cantidadRemolques = cantidadRemolques;
    }

    public Sonido getSonido() {
        return sonido;
    }

    public void setSonido(Sonido sonido) {
        this.sonido = sonido;
    }
}
