package modelos.vehiculo.clases;

import modelos.vehiculo.enums.Sonido;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class SedanElectrico extends Electrico {
    private Double autonomia;
    private Boolean simulaSonido;
    private Sonido sonido;

    public SedanElectrico(){

    }

    public SedanElectrico(Double autonomia, Boolean simulaSonido, Sonido sonido) {
        this.autonomia = autonomia;
        this.simulaSonido = simulaSonido;
        this.sonido = sonido;
    }

    public SedanElectrico(Integer numeroLlantas, Float cilindraje, Float tanque, Integer numeroPuertas, Integer numeroPasajeros, Double autonomia, Boolean simulaSonido, Sonido sonido) {
        super(numeroLlantas, cilindraje, tanque, numeroPuertas, numeroPasajeros);
        this.autonomia = autonomia;
        this.simulaSonido = simulaSonido;
        this.sonido = sonido;
    }

    public Double getAutonomia() {
        return autonomia;
    }

    public void setAutonomia(Double autonomia) {
        this.autonomia = autonomia;
    }

    public Boolean getSimulaSonido() {
        return simulaSonido;
    }

    public void setSimulaSonido(Boolean simulaSonido) {
        this.simulaSonido = simulaSonido;
    }

    public Sonido getSonido() {
        return sonido;
    }

    public void setSonido(Sonido sonido) {
        this.sonido = sonido;
    }
}
