package modelos.vehiculo.clases;

import modelos.vehiculo.excepciones.TanqueLlenoExpection;
import modelos.vehiculo.excepciones.YaEnEseEstadoException;
import modelos.vehiculo.interfaces.Energizable;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class Electrico extends Vehiculo implements Energizable{

    public Electrico() {
    }

    public Electrico(Integer numeroLlantas, Float cilindraje, Float tanque, Integer numeroPuertas, Integer numeroPasajeros) {
        super(numeroLlantas, cilindraje, tanque, numeroPuertas, numeroPasajeros);
    }

    @Override
    public void setTanqueCapacidad(Float tanqueCapacidad) {
        super.setTanqueCapacidad(1.00f);
    }

    @Override
    public void llenar() throws TanqueLlenoExpection {
        if (tanqueLleno()) throw new TanqueLlenoExpection();
        setTanque(1.00f);
    }

    @Override
    public Boolean tanqueVacio() {
        return getTanque() == 0.0f? Boolean.TRUE : Boolean.FALSE;
    }

    @Override
    public Boolean tanqueLleno() {
        return getTanque() == 1.00f ? Boolean.TRUE : Boolean.FALSE;
    }

    @Override
    public void arrancar() {
        System.out.println("*Sin ruido*");
    }

    @Override
    public void apagar() throws YaEnEseEstadoException {
        if ( getEncendido().equals(Boolean.FALSE) ) throw new YaEnEseEstadoException();
        setEncendido(Boolean.FALSE);
    }

    @Override
    public void avanzar() {
        System.out.println("*Sonido de llanantas apenas percibible*");
    }

    @Override
    public void encender() throws YaEnEseEstadoException{
        if( getEncendido().equals(Boolean.TRUE) ) throw new YaEnEseEstadoException();
        setEncendido(Boolean.TRUE);
        System.out.println("*Sonido similar a un vehiculo fosil con el fin de que el conductor no se vuevla paranoico*");
    }

}
