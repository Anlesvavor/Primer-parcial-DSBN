package modelos.vehiculo.clases;


import modelos.vehiculo.enums.Sonido;
import modelos.vehiculo.enums.TIpo;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 15/03/18
 */
public class CarritoRC extends SedanElectrico {
    private Double Rango;
    private Double Peso;
    private TIpo tipo;

    public CarritoRC(Double rango, Double peso, TIpo tipo) {
        Rango = rango;
        Peso = peso;
        this.tipo = tipo;
    }

    public CarritoRC(Double autonomia, Boolean simulaSonido, Sonido sonido, Double rango, Double peso, TIpo tipo) {
        super(autonomia, simulaSonido, sonido);
        Rango = rango;
        Peso = peso;
        this.tipo = tipo;
    }

    public CarritoRC(Integer numeroLlantas, Float cilindraje, Float tanque, Integer numeroPuertas, Integer numeroPasajeros, Double autonomia, Boolean simulaSonido, Sonido sonido, Double rango, Double peso, TIpo tipo) {
        super(numeroLlantas, cilindraje, tanque, numeroPuertas, numeroPasajeros, autonomia, simulaSonido, sonido);
        Rango = rango;
        Peso = peso;
        this.tipo = tipo;
    }

    public Double getRango() {
        return Rango;
    }

    public void setRango(Double rango) {
        Rango = rango;
    }

    public Double getPeso() {
        return Peso;
    }

    public void setPeso(Double peso) {
        Peso = peso;
    }

    public TIpo getTipo() {
        return tipo;
    }

    public void setTipo(TIpo tipo) {
        this.tipo = tipo;
    }
}

