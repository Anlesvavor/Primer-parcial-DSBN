package modelos.vehiculo.interfaces;

import modelos.vehiculo.excepciones.TanqueLlenoExpection;

public interface Energizable {

    public void llenar() throws TanqueLlenoExpection;
    public Boolean tanqueVacio();
    public Boolean tanqueLleno();
}
