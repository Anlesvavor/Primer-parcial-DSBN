package modelos.refrigerador.interfaces;

import java.util.Date;

public interface Perecedero {

    public Date fechaDeElaboraicon = new Date();
    

    public void setFechaDeElaboracion(Date fechaDeElaboracion);
    public Date getFechaDeElaboracion();

    public void setFechaDeCaducidad(Date fechaDeCaducidad);
    public Date getFechaDeCaducidad();

    public void caducir();

}
