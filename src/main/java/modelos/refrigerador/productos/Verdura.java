package modelos.refrigerador.productos;

import modelos.refrigerador.enums.Medida;
import modelos.refrigerador.interfaces.Perecedero;

import java.awt.*;
import java.util.Date;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 11/03/18
 */
public class Verdura extends Producto implements Perecedero{

    private String nombre;
    private String descripcion;
    private Date fechaDeCultivo;
    private Color color;
    private Double peso;

    private Date fechaDeElaboracion;
    private Date fechaDeCaducidad;

    public Verdura(String nombre, Double cantidad, Medida medida, String descripcion, String marca, String nombre1, String descripcion1, Date fechaDeCultivo, Color color, Double peso, Date fechaDeElaboracion, Date fechaDeCaducidad) {
        super(nombre, cantidad, medida, descripcion, marca);
        this.nombre = nombre1;
        this.descripcion = descripcion1;
        this.fechaDeCultivo = fechaDeCultivo;
        this.color = color;
        this.peso = peso;
        this.fechaDeElaboracion = fechaDeElaboracion;
        this.fechaDeCaducidad = fechaDeCaducidad;
    }

    public Verdura() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaDeCultivo() {
        return fechaDeCultivo;
    }

    public void setFechaDeCultivo(Date fechaDeCultivo) {
        this.fechaDeCultivo = fechaDeCultivo;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Double getPeso() {
        return peso;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }



    public void setFechaDeElaboracion(Date fechaDeElaboracion) {
        this.fechaDeElaboracion = fechaDeElaboracion;
    }

    public Date getFechaDeElaboracion() {
        return null;
    }

    public void setFechaDeCaducidad(Date fechaDeCaducidad) {
        this.fechaDeCaducidad = fechaDeCaducidad;
    }

    public Date getFechaDeCaducidad() {
        return null;
    }

    public void caducir() {
        System.out.println("Germina o huele feo");
    }
}
