package modelos.refrigerador.productos;

import modelos.refrigerador.enums.Medida;
import modelos.refrigerador.interfaces.Perecedero;

import java.util.Date;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 10/03/18
 */
public class Lacteo extends Producto implements Perecedero {

    private Float lechosidad;
    private Boolean esDeslactosado;
    private Date fechaDeElaboracion;
    private Date fechaDeCaducidad;

    public Lacteo() {
    }

    public Lacteo(String nombre, Double cantidad, Medida medida, String descripcion, String marca, Float lechosidad, Boolean esDeslactosado, Date fechaDeElaboracion, Date fechaDeCaducidad) {
        super(nombre, cantidad, medida, descripcion, marca);
        this.lechosidad = lechosidad;
        this.esDeslactosado = esDeslactosado;
        this.fechaDeElaboracion = fechaDeElaboracion;
        this.fechaDeCaducidad = fechaDeCaducidad;
        calcularLechosidad();
    }

    private void calcularLechosidad() {
        if(this.esDeslactosado.equals(Boolean.TRUE)) this.lechosidad = 0.0f;
    }

    public Float getLechosidad() {
        return lechosidad;
    }

    public void setLechosidad(Float lechosidad) {
        this.lechosidad = lechosidad;
    }

    public Boolean getEsDeslactosado() {
        return esDeslactosado;
    }

    public void setEsDeslactosado(Boolean esDeslactosado) {
        this.esDeslactosado = esDeslactosado;
    }

    public void setFechaDeElaboracion(Date fechaDeElaboracion) {
        this.fechaDeElaboracion = fechaDeElaboracion;
    }

    public Date getFechaDeElaboracion() {
        return this.fechaDeElaboracion;
    }

    public void setFechaDeCaducidad(Date fechaDeCaducidad) {
        this.fechaDeCaducidad = fechaDeCaducidad;
    }

    public Date getFechaDeCaducidad() {
        return this.fechaDeCaducidad;
    }

    public void caducir() {
        System.out.println("*Se pone agrio*");
    }
}
