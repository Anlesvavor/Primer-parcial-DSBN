package modelos.refrigerador.productos;


import modelos.refrigerador.enums.Categoria;

import java.util.Date;
import java.util.List;


/**
 * @Author Jesús José Sáenz Fierro
 * @Date 10/03/18
 */
public class Refrigerador extends Electrodomestico {

    private Double temperatura;
    private Double temperaturaMinima;
    private Double temperaturaMaxima;
    private Double volumen;
    private List<Producto> productos;


    public static void main(String... args) {
        System.out.println("Conectar al tomacorriente");
        System.out.println("Encendido");
    }

    public Refrigerador() {

    }

    public Refrigerador(Double voltage, Categoria categoria, Date fechaManufactura, String marca) {
        super(voltage, categoria, fechaManufactura, marca);
    }

    public Refrigerador(Double voltage, Categoria categoria, Date fechaManufactura, String marca, Double temperatura, Double temperaturaMinima, Double temperaturaMaxima, Double volumen, List<Producto> productos) {
        super(voltage, categoria, fechaManufactura, marca);
        this.temperatura = temperatura;
        this.temperaturaMinima = temperaturaMinima;
        this.temperaturaMaxima = temperaturaMaxima;
        this.volumen = volumen;
        this.productos = productos;
    }

    public Double getTemperatura() {

        return temperatura;
    }

    public void setTemperatura(Double temperatura) {
        this.temperatura = temperatura;
    }

    public Double getTemperaturaMinima() {
        return temperaturaMinima;
    }

    public void setTemperaturaMinima(Double temperaturaMinima) {
        this.temperaturaMinima = temperaturaMinima;
    }

    public Double getTemperaturaMaxima() {
        return temperaturaMaxima;
    }

    public void setTemperaturaMaxima(Double temperaturaMaxima) {
        this.temperaturaMaxima = temperaturaMaxima;
    }

    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    public void agregarProducto(Producto producto){
        this.productos.add(producto);
    }

    public void sacarProducto(Producto producto){
        this.productos.remove(producto);
    }

    public void encender(){
        main();
    }

    public void apagar(){
        System.out.println("Apagado");
    }

    public void reiniciar(){
        apagar();
        encender();
    }

}
