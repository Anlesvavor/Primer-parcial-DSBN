package modelos.refrigerador.productos;

import modelos.refrigerador.enums.Medida;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 10/03/18
 */
public class Producto {

    private String nombre;
    private Double cantidad;
    private Medida medida;
    private String descripcion;
    private String marca;

    public Producto() {
    }

    public Producto(String nombre, Double cantidad, Medida medida, String descripcion, String marca) {
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.medida = medida;
        this.descripcion = descripcion;
        this.marca = marca;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public Medida getMedida() {
        return medida;
    }

    public void setMedida(Medida medida) {
        this.medida = medida;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }
}
