package modelos.refrigerador.productos;

import modelos.refrigerador.enums.Categoria;

import java.util.Date;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 11/03/18
 */
public abstract class Electrodomestico {

    private Double voltage;
    private Categoria categoria;
    private Date fechaManufactura;
    private String marca;

    public Double getVoltage() {
        return voltage;
    }

    public void setVoltage(Double voltage) {
        this.voltage = voltage;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Date getFechaManufactura() {
        return fechaManufactura;
    }

    public void setFechaManufactura(Date fechaManufactura) {
        this.fechaManufactura = fechaManufactura;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Electrodomestico(Double voltage, Categoria categoria, Date fechaManufactura, String marca) {

        this.voltage = voltage;
        this.categoria = categoria;
        this.fechaManufactura = fechaManufactura;
        this.marca = marca;
    }

    public Electrodomestico() {

    }

    public void encender(){

    }

    public void apagar(){

    }


}
