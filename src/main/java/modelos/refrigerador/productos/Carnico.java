package modelos.refrigerador.productos;

import modelos.refrigerador.interfaces.Perecedero;

import java.util.Date;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 11/03/18
 */
public class Carnico extends Producto implements Perecedero {


    public void setFechaDeElaboracion(Date fechaDeElaboracion) {

    }

    public Date getFechaDeElaboracion() {
        return null;
    }

    public void setFechaDeCaducidad(Date fechaDeCaducidad) {

    }

    public Date getFechaDeCaducidad() {
        return null;
    }

    public void caducir() {

    }
}
