package modelos.refrigerador.enums;

public enum Medida {
    MILILITROS, LITROS, GRAMOS, KILOGRAMOS, GALONES, ONZAS, PIEZAS;
}
