package modelos.refrigerador.enums;

public enum Carne {

    POLLO(Boolean.FALSE, Boolean.TRUE, Boolean.FALSE),
    CERDO(Boolean.TRUE, Boolean.FALSE, Boolean.FALSE),
    PAVO(Boolean.FALSE, Boolean.TRUE, Boolean.FALSE),
    RES(Boolean.TRUE, Boolean.FALSE, Boolean.FALSE),
    PESCADO(Boolean.FALSE, Boolean.TRUE, Boolean.FALSE),
    CERDOYPAVO(Boolean.FALSE, Boolean.FALSE, Boolean.TRUE),
    CABALLO(Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);

    private Boolean esCarneRoja;
    private Boolean esCarneBlanca;

    Carne(Boolean esCarneRoja, Boolean esCarneBlanca, Boolean esCombinado) {
        this.esCarneRoja = esCarneRoja;
        this.esCarneBlanca = esCarneBlanca;
        this.esCombinado = esCombinado;
    }

    public Boolean getEsCarneRoja() {
        return esCarneRoja;
    }

    public Boolean getEsCarneBlanca() {
        return esCarneBlanca;
    }

    public Boolean getEsCombinado() {
        return esCombinado;
    }

    private Boolean esCombinado;

}
