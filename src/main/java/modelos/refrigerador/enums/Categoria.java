package modelos.refrigerador.enums;

/**
 * @Author Jesús José Sáenz Fierro
 * @Date 11/03/18
 */
public enum Categoria {
    LINEAMARRON,
    LINEABLANCA,
    PAE;
}
